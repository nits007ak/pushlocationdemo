
//  Created by CRATERZONE on 28/12/13.
//  Copyright (c) 2013 CRATERZONE. All rights reserved.
//
#import "SampleAppDelegate.h"

#import "UAConfig.h"
#import "UAirship.h"
#import "UAPush.h"
#import "UAAnalytics.h"
#import "ASIHTTPRequest.h"
#import "InternetUtility.h"

#define UPDATE_LOCATION_SERVICE @"http://54.219.137.250/locationUpdateDemo/location/update?"
#define HTTP_REQUEST_GET_METHOD @"GET"
#define HTTP_REQUEST_HEADER @"Accept"
#define HTTP_REQUEST_HEADER_VALUE @"application/json"
#define HTTP_REQUEST_CONTENTTYPE @"content-type"
#define HTTP_REQUEST_CONTENTTYPE_VALUE @"application/json"
#define USER_AGENT @"User-Agent"
#define DEVICE @"iPhone"

@interface SampleAppDelegate() {
    NSDate* _lastSentUpdateAt;
    NSTimer *timer;
    UIBackgroundTaskIdentifier bgTask;
    BOOL isLocationServiceStart;
}
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation SampleAppDelegate
@synthesize locationManager;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

//    [self application:application didReceiveRemoteNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
//    NSDictionary *notif= [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
//    if(notif) {
//        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
//        if (localNotif) {
//            localNotif.alertBody = @"body in background";
//            localNotif.alertAction = @"Notification Location";
//            localNotif.soundName = @"frog.caf";
//            localNotif.applicationIconBadgeNumber = 5;
//            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
//        }
//    }
    
    
    // Override point for customization after application launch
    [self.window setRootViewController:_controller];
    [self.window makeKeyAndVisible];

    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    isLocationServiceStart = YES;
    timer = [NSTimer scheduledTimerWithTimeInterval:180
                                             target:self
                                           selector:@selector(startUpdatingLocation)
                                           userInfo:nil
                                            repeats:YES];
    
    // Display a UIAlertView warning developers that push notifications do not work in the simulator
    [self failIfSimulator];
    // Urban airship configuration
    [UAPush setDefaultPushEnabledValue:NO];

    [UAirship setLogLevel:UALogLevelTrace];

    UAConfig *config = [UAConfig defaultConfig];

    [UAirship takeOff:config];
    
    UA_LDEBUG(@"Config:\n%@", [config description]);

    // Set the icon badge to zero on startup (optional)
    [[UAPush shared] resetBadge];
    
    //register app for remote notification
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert);

    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    UA_LDEBUG(@"Application did become active.");
    
    // Set the icon badge to zero on resume (optional)
    [[UAPush shared] resetBadge];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //[locationManager stopUpdatingLocation];
    UIApplication*    app = [UIApplication sharedApplication];
    
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
//    timer = [NSTimer scheduledTimerWithTimeInterval:180
//                                             target:self
//                                           selector:@selector(startUpdatingLocation)
//                                           userInfo:nil
//                                            repeats:YES];
}

#pragma mark - location delegate methods

/*
 This method starts location service after every 60 seconds.
 And calls delegate method of location manager.
 */
-(void)startUpdatingLocation {
    [locationManager stopUpdatingLocation];
    NSLog(@"start updating");
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    isLocationServiceStart = YES;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if(isLocationServiceStart) {
        currentLocation = newLocation;
        [self sendDataToServer:newLocation];
        isLocationServiceStart = NO;
    }
    
}

-(void) showAlertView {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)                                                                                            message:NSLocalizedString(@"Could not connect to server", nil)
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    [alertView show];
}

#pragma mark - web Service call method

/* This method sends updated latitude and longitude to server 
 params : location object (newLocation)
 */
-(void) sendDataToServer:(CLLocation *)newLocation
{
    NSString *userId = @"134";
    NSString *latitude = [NSString stringWithFormat:@"%.8f", newLocation.coordinate.longitude];;
    NSString *longitude = [NSString stringWithFormat:@"%.8f", newLocation.coordinate.latitude];
    
    NSLog(@"Sending Data to Server");
    if([InternetUtility testInternetConnection]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@userID=%@&latitude=%@&longitude=%@",UPDATE_LOCATION_SERVICE,userId,latitude,longitude]];
        responseData = [[NSMutableData alloc] init];
        if(url != nil) {
            ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            //NSString *jsonRequest = [upgradeDict JSONRepresentation];
            //[request appendPostData:[jsonRequest dataUsingEncoding:NSUTF8StringEncoding]];
            //NSLog(@"jsonRequest is %@", request);
            [request setRequestMethod:HTTP_REQUEST_GET_METHOD];
            [request addRequestHeader:HTTP_REQUEST_HEADER value:HTTP_REQUEST_HEADER_VALUE];
            [request addRequestHeader:HTTP_REQUEST_CONTENTTYPE value:HTTP_REQUEST_CONTENTTYPE_VALUE];
            [request addRequestHeader:USER_AGENT value:DEVICE];
            [request setDelegate:self];
            [request setCompletionBlock:^{
                NSString *responseString = [request responseString];
                //NSLog(@"responseDict = %@",responseDict);
                //                UILocalNotification *localNotif = [[UILocalNotification alloc] init];
                //                if (localNotif) {
                //                    localNotif.alertBody = @"body test";
                //                    localNotif.alertAction = @"Notification Location";
                //                    localNotif.soundName = @"pig.caf";
                //                    localNotif.applicationIconBadgeNumber = 5;
                //                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
                //                }
                NSLog(@"data send to server");
            }];
            [request setFailedBlock:^{
                NSLog (@"Unexpected error");
            }];
            [request startAsynchronous];
        }
    }
    //implement operation we need to do with new location object.
}


#pragma mark - Remote Notification Delegate methods.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@",error);
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
        NSString *deviceTokenString = [NSString stringWithFormat:@"%@",deviceToken];
        NSString *sub = [deviceTokenString stringByReplacingOccurrencesOfString:@">" withString:@""];
        sub = [sub stringByReplacingOccurrencesOfString:@"<" withString:@""];
        sub = [sub stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    UA_LINFO(@"Received remote notification (in appDelegate): %@", userInfo);

    // Reset the badge after a push received (optional)
    [[UAPush shared] resetBadge];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    UA_LINFO(@"Received remote notification (in appDelegate): %@", userInfo);

    
    // Reset the badge after a push is received in a active or inactive state
    //if (application.applicationState != UIApplicationStateBackground) {
        
        isLocationServiceStart = YES;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
    
//        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
//        if (localNotif) {
//            localNotif.alertBody = @"body in background";
//            localNotif.alertAction = @"Notification Location";
//            localNotif.soundName = @"frog.caf";
//            localNotif.applicationIconBadgeNumber = 5;
//            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
//        }
    
//        bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
//        }];
//        
//        // Send the data
//        isLocationServiceStart = YES;
//        locationManager = [[CLLocationManager alloc] init];
//        locationManager.delegate = self;
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//        locationManager.distanceFilter = kCLDistanceFilterNone;
//        [locationManager startUpdatingLocation];
//        
//        if (bgTask != UIBackgroundTaskInvalid) {
//            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
//            bgTask = UIBackgroundTaskInvalid;
//        }
        
        
        [[UAPush shared] resetBadge];
   // }

    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)launchedFromNotification:(NSDictionary *)notification
          fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    UA_LDEBUG(@"The application was launched or resumed from a notification");
    
    // Do something when launched via a notification
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif) {
        localNotif.alertBody = @"body in background";
        localNotif.alertAction = @"Notification Location";
        localNotif.soundName = @"frog.caf";
        localNotif.applicationIconBadgeNumber = 5;
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    }
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)receivedBackgroundNotification:(NSDictionary *)notification
                fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Do something with the notification in the background
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif) {
        localNotif.alertBody = @"body in background";
        localNotif.alertAction = @"Notification Location";
        localNotif.soundName = @"frog.caf";
        localNotif.applicationIconBadgeNumber = 5;
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    }
    
    // Be sure to call the completion handler with a UIBackgroundFetchResult
    completionHandler(UIBackgroundFetchResultNoData);
}



- (void)failIfSimulator {
    if ([[[UIDevice currentDevice] model] rangeOfString:@"Simulator"].location != NSNotFound) {
        UIAlertView *someError = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                            message:@"You will not be able to receive push notifications in the simulator."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];

        // Let the UI finish launching first so it doesn't complain about the lack of a root view controller
        // Delay execution of the block for 1/2 second.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [someError show];
        });

    }
}



@end
