
//  Created by CRATERZONE on 28/12/13.
//  Copyright (c) 2013 CRATERZONE. All rights reserved.
//
#import "SampleViewController.h"
#import "UAPush.h"
#import "UAirship.h"
#import "UAPushUI.h"


@implementation SampleViewController

- (IBAction)buttonPressed:(id)sender {
    if (sender == self.settingsButton) {
        [UAPush openApnsSettings:self animated:YES];
    } else if (sender == self.tokenButton) {
        [UAPushUI openTokenSettings:self animated:YES];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.version.text = @"settings";
    [UAPush useCustomUI:[UAPushUI class]];
}

@end
