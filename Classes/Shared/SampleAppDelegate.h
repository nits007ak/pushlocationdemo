
//
//  Created by CRATERZONE on 28/12/13.
//  Copyright (c) 2013 CRATERZONE. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SampleAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate> {

    NSMutableData *responseData;
    CLLocation *currentLocation;
}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UIViewController *controller;

- (void)failIfSimulator;

@end

