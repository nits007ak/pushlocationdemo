
//  Created by CRATERZONE on 28/12/13.
//  Copyright (c) 2013 CRATERZONE. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface SampleViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *settingsButton;
@property (nonatomic, strong) IBOutlet UIButton *tokenButton;
@property (nonatomic, strong) IBOutlet UILabel *version;

- (IBAction)buttonPressed:(id)sender;

@end
